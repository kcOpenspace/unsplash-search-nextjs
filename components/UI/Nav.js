import navStyles from '../../styles/Nav.module.css'
import Link from 'next/link'

const Nav = () => {
  return (
    <nav className={navStyles.nav}>
      <ul className={navStyles.nav_list}>
        <li className={navStyles.list_item}>
          <Link href="/">Home</Link>
        </li>
        <li className={navStyles.list_item}>
          <Link href="/collection">Collection</Link>
        </li>

      </ul>
    </nav>
  );
};

export default Nav;