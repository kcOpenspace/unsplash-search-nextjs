import headerStyles from '../../styles/Header.module.css'

const Header = () => {
  return (
    <div className={headerStyles.header_wrap}>
      <h1 className={headerStyles.title}>
        <span>Unsplash</span> API Image Search
      </h1>
      <p className={headerStyles.p}>Built with Next JS</p>
    </div>
  );
};

export default Header;