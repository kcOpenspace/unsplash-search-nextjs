import { useState, useEffect } from 'react'

const SearchBar = ({ getQuery }) => {

  const [input, setInput] = useState('')

  const search = (evt) => {
    if (evt.key === "Enter") {
      console.log(input)
      const q = input
      getQuery(q)
    }  
  }

  return (
    <div className="inputWrap">
      <label htmlFor="query">** Hit Enter to Search **</label>
      <input
        className="searchBox"
        type="text"
        name="query"
        placeholder="Enter search here ..."
        autoFocus
        value={input}
        onChange={(e) => setInput(e.target.value)}
        onKeyPress={search}
      />
    </div>
  );
};

export default SearchBar;