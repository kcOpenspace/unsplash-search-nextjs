import Link from 'next/link'
import Image from 'next/image'
import imageStyles from '../../styles/Images.module.css'

const ImageItem = ({ image }) => {
  return (
    <div className={imageStyles.image_item_wrap}>
      <Link href="/image/[id]" as={`/image/${image.id}`}>
        <a className={imageStyles.image_wrap}>
          <Image className={imageStyles.image} src={image.urls.regular} width="500" height="400" objectFit="cover" />
          {/* <p>{image.description}</p> */}
        </a>
      </Link>
    </div>
  );
};

export default ImageItem;