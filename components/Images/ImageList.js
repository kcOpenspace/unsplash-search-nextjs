import imageStyles from '../../styles/Images.module.css'
import ImageItem from './ImageItem'

const ImageList = ({ images, isLoading }) => {
  return isLoading ? (
    <div className={imageStyles.image_list_wrap}>
      <h3>Loading...</h3>
    </div>
  ):(
    <div className={imageStyles.image_list_wrap}>
      {images.map((image) => (
        <div key={image.id} className={imageStyles.image_card}>
          <ImageItem image={image} />
        </div>
      ))}
    </div>
  );
};

export default ImageList;