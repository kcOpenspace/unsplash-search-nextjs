import Image from 'next/image'
import Link from 'next/link'
import imageStyles from '../../../styles/Images.module.css'

const image = ({ image }) => {
  return (
    <div className={imageStyles.single_image_page}>
      <div className={imageStyles.image_container}>
        <div className={imageStyles.image}>
          <Image src={image.urls.regular} width="600" height="400" objectFit="cover"/>
        </div>

        <div className={imageStyles.image_content}>
          {image.tags_preview && image.tags_preview.length ?
          (<ul className={imageStyles.tag_list}>
            <span>Tags</span>
            {image.tags_preview.map((tag,i) => (
              <li key={i} className={imageStyles.tag}>{tag.title}</li>
            ))}
          </ul>)
          : ' '}

          {/* TO DO : Data validation and checks */}

          <h3>Photo taken by {image.user.first_name ? image.user.first_name : 'Unknown'}</h3>
          <p className="dimensions">Image dimensions {image.width} x {image.height}</p>
          <Link href={image.links.download}><a className="button">Link to Download Image</a></Link>
        </div>
      </div>
    </div>
  );
};

export const getServerSideProps = async (context) => {

  const api_key = process.env.NEXT_PUBLIC_API_KEY
  
  const res = await fetch(`https://api.unsplash.com/photos/${context.params.id}?client_id=${api_key}`)
  const image = await res.json()

  return {
    props: {
      image
    }
  }
}

export default image;