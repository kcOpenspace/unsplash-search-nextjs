import {server} from '../config'
import Head from 'next/head'
import ImageList from '../components/Images/ImageList'
import Link from 'next/link'

export default function Home({ images }) {
  return (
    <div>
      <div className="container">
        <Link href="/collection"><a className="button">Click to search unsplash collection for an image</a></Link>
      </div>
      <ImageList images={images} />    
    </div>
  )
}

export const getStaticProps = async () => {
  const api_key = process.env.NEXT_PUBLIC_API_KEY
  const res = await fetch(`https://api.unsplash.com/search/photos?query=dogs&page=1&per_page=6&client_id=${api_key}`)
  const images = await res.json()
  return {
    props: {
      images: images.results
    },
    revalidate: 30
  }
}