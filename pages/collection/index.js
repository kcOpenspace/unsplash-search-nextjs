import { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import SearchBar from '../../components/UI/SearchBar'
import ImageList from '../../components/Images/ImageList'
import Meta from '../../components/UI/Meta'


const CollectionPage = () => {

  const [data, setData] = useState('')
  const [isLoading, setLoading] = useState(true)
  const [query, setQuery] = useState('cats')

  useEffect(() => {
    const fetchItems = async () => {
      const api_key = process.env.NEXT_PUBLIC_API_KEY

      try {
        const res = await fetch(`https://api.unsplash.com/search/photos?page=1&per_page=12&query=${query}&client_id=${api_key}`)
        const images = await res.json()
    
        setData(images.results)
        setLoading(false)

      } catch (err) {
        console.alert('API Fetch Request Error: Possible 50 requests per hour limit reached, try again next hour.');
      }
    }    
    fetchItems()
  },[query])


  return (
    <div>
      <Meta title='Collection' description='Collection page to search the Unsplash api' />
      <h1>Collection Search Page</h1>
      <SearchBar getQuery={(q) => setQuery(q)}/>
      <div className="image-wrap">
        {data ? 
          <ImageList isLoading={isLoading} images={data}/>
          : 'No data or api 50 requests/hour limit has been reached'}
      </div> 
    </div>
  );

};


// export const getStaticProps = async () => {
//   const api_key = process.env.NEXT_PUBLIC_API_KEY
//   const res = await fetch(`https://api.unsplash.com/search/photos?page=1&per_page=3&query=cats&client_id=${api_key}`)
//   const images = await res.json()
//   return {
//     props: {
//       images: images.results
//     }
//   }
// }


export default CollectionPage;